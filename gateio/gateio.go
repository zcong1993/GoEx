package gateio

import (
	"crypto/hmac"
	"crypto/sha512"
	"encoding/json"
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	. "gitlab.com/zcong1993/GoEx"
	"net/http"
	"sort"
	"strconv"
	"strings"
)

const EXCHANGE_NAME = "gate.io"

var (
	marketBaseUrl = "http://data.gate.io/api2/1"
	privateUrl    = marketBaseUrl + "/private/"
	TRUE          = "true"
	ErrBadData    = errors.New("api response data is not ok")
)

type Gate struct {
	client *http.Client
	accesskey,
	secretkey string
}

type params map[string]string

const (
	BUYORDER = 1 + iota
	SELLORDER
	ALLORDER
)

func New(client *http.Client, accesskey, secretkey string) *Gate {
	return &Gate{client: client, accesskey: accesskey, secretkey: secretkey}
}

func (g *Gate) sign(params string) string {
	key := []byte(g.secretkey)
	mac := hmac.New(sha512.New, key)
	mac.Write([]byte(params))
	return fmt.Sprintf("%x", mac.Sum(nil))
}

func BuildQuery(p params) string {
	q := ""
	for k, v := range p {
		q += fmt.Sprintf("&%s=%s", k, v)
	}
	if q == "" {
		return q
	}
	return q[1:]
}

func (g *Gate) normalizeCurrency(currency CurrencyPair) string {
	return strings.ToLower(currency.ToSymbol("_"))
}
func (g *Gate) normalizeOrderSide(side string) TradeSide {
	switch side {
	case "sell":
		return SELL
	case "buy":
		return BUY
	default:
		return -100
	}
}

func (g *Gate) doPrivateApi(url string, p params) (map[string]interface{}, error) {
	query := BuildQuery(p)
	headers := map[string]string{
		"Content-Type": "application/x-www-form-urlencoded",
		"key":          g.accesskey,
		"sign":         g.sign(query),
	}
	data, err := NewHttpRequest(g.client, "POST", url, query, headers)
	if err != nil {
		return nil, err
	}
	var d map[string]interface{}
	err = json.Unmarshal(data, &d)
	if err != nil {
		println(string(data))
		return nil, err
	}
	log.Debug(d)
	return d, nil
}

func (g *Gate) makeOrder(side int, amount, price string, currency CurrencyPair) (*Order, error) {
	p := params{
		"rate":         price,
		"amount":       amount,
		"currencyPair": g.normalizeCurrency(currency),
	}
	url := privateUrl
	switch side {
	case SELLORDER:
		url += "sell"
	case BUYORDER:
		url += "buy"
	}
	data, err := g.doPrivateApi(url, p)
	if err != nil {
		return nil, err
	}
	if data["result"].(string) != TRUE {
		log.Debug(data["message"].(string))
		return nil, ErrBadData
	}

	order := new(Order)
	order.OrderID = ToInt(data["orderNumber"])
	order.Amount, _ = strconv.ParseFloat(amount, 64)
	order.Price, _ = strconv.ParseFloat(price, 64)
	order.Status = ORDER_UNFINISH
	order.Currency = currency

	switch side {
	case SELLORDER:
		order.Side = SELL
	case BUYORDER:
		order.Side = BUY
	}

	return order, nil
}

func (g *Gate) LimitBuy(amount, price string, currency CurrencyPair) (*Order, error) {
	return g.makeOrder(BUYORDER, amount, price, currency)
}
func (g *Gate) LimitSell(amount, price string, currency CurrencyPair) (*Order, error) {
	return g.makeOrder(SELLORDER, amount, price, currency)
}
func (g *Gate) MarketBuy(amount, price string, currency CurrencyPair) (*Order, error) {
	panic("not implement")
}
func (g *Gate) MarketSell(amount, price string, currency CurrencyPair) (*Order, error) {
	panic("not implement")
}
func (g *Gate) CancelOrder(orderId string, currency CurrencyPair) (bool, error) {
	p := params{
		"orderNumber":  orderId,
		"currencyPair": g.normalizeCurrency(currency),
	}
	url := privateUrl + "cancelOrder"
	data, err := g.doPrivateApi(url, p)
	if err != nil {
		return false, err
	}
	if _, ok := data["result"].(bool); ok {
		return true, nil
	}
	log.Debug(data["message"].(string))
	return false, nil
}

func (g *Gate) GetOneOrder(orderId string, currency CurrencyPair) (*Order, error) {
	p := params{
		"orderNumber":  orderId,
		"currencyPair": g.normalizeCurrency(currency),
	}
	url := privateUrl + "getOrder"
	data, err := g.doPrivateApi(url, p)
	if err != nil {
		return nil, err
	}
	if data["result"].(string) != TRUE {
		return nil, ErrBadData
	}
	orderMap := data["order"].(map[string]interface{})
	orderNumber := orderMap["id"].(string)
	order := new(Order)
	order.OrderID, _ = strconv.Atoi(orderNumber)
	order.Currency = currency
	order.CurrencyRaw = orderMap["currencyPair"].(string)
	order.Price = ToFloat64(orderMap["rate"])
	order.Amount = ToFloat64(orderMap["amount"])
	order.Side = g.normalizeOrderSide(orderMap["side"].(string))

	return order, nil
}

func (g *Gate) GetUnfinishOrders(currency CurrencyPair) ([]Order, error) {
	p := params{}
	url := privateUrl + "openOrders"
	data, err := g.doPrivateApi(url, p)
	if err != nil {
		return nil, err
	}
	if data["result"].(string) != TRUE {
		return nil, ErrBadData
	}
	ordersArr, _ := data["orders"].([]interface{})
	var orders []Order
	for _, v := range ordersArr {
		vv, _ := v.(map[string]interface{})
		order := Order{}
		order.OrderID, _ = strconv.Atoi(vv["orderNumber"].(string))
		order.Price = ToFloat64(vv["rate"])
		order.Amount = ToFloat64(vv["amount"])
		order.OrderTime, _ = strconv.Atoi(vv["timestamp"].(string))
		order.CurrencyRaw = vv["currencyPair"].(string)
		orders = append(orders, order)
	}
	return orders, nil
}
func (g *Gate) GetOrderHistorys(currency CurrencyPair, currentPage, pageSize int) ([]Order, error) {
	return nil, nil
}
func (g *Gate) GetAccount() (*Account, error) {
	p := params{}
	url := privateUrl + "balances"
	data, err := g.doPrivateApi(url, p)
	if err != nil {
		return nil, err
	}
	if data["result"].(string) != TRUE {
		return nil, ErrBadData
	}
	acc := new(Account)
	acc.Exchange = EXCHANGE_NAME
	acc.SubAccounts = make(map[Currency]SubAccount)
	available := data["available"].(map[string]interface{})
	for k, v := range available {
		var currency Currency

		switch strings.ToUpper(k) {
		case "BTC":
			currency = BTC
		case "LTC":
			currency = LTC
		case "ETH":
			currency = ETH
		case "ETC":
			currency = ETC
		case "USD":
			currency = USD
		case "USDT":
			currency = USD
		case "BCH":
			currency = BCC

		default:
			currency = UNKNOWN
		}

		if currency != UNKNOWN {
			subAcc := SubAccount{}
			subAcc.Currency = currency
			subAcc.Amount = ToFloat64(v)
			acc.SubAccounts[subAcc.Currency] = subAcc
		}
	}
	locked := data["locked"].(map[string]interface{})
	for k, v := range locked {
		var currency Currency

		switch strings.ToUpper(k) {
		case "BTC":
			currency = BTC
		case "LTC":
			currency = LTC
		case "ETH":
			currency = ETH
		case "ETC":
			currency = ETC
		case "USD":
			currency = USD
		case "USDT":
			currency = USD
		case "BCH":
			currency = BCC

		default:
			currency = UNKNOWN
		}

		if currency != UNKNOWN {
			subAcc := acc.SubAccounts[currency]
			subAcc.ForzenAmount = ToFloat64(v)
		}
	}
	return acc, nil
}

func (g *Gate) GetTicker(currency CurrencyPair) (*Ticker, error) {
	uri := fmt.Sprintf("%s/ticker/%s", marketBaseUrl, strings.ToLower(currency.ToSymbol("_")))

	resp, err := HttpGet(g.client, uri)
	if err != nil {
		errCode := HTTP_ERR_CODE
		errCode.OriginErrMsg = err.Error()
		return nil, errCode
	}

	return &Ticker{
		Last: ToFloat64(resp["last"]),
		Sell: ToFloat64(resp["lowestAsk"]),
		Buy:  ToFloat64(resp["highestBid"]),
		High: ToFloat64(resp["high24hr"]),
		Low:  ToFloat64(resp["low24hr"]),
		Vol:  ToFloat64(resp["quoteVolume"]),
	}, nil
}

func (g *Gate) GetDepth(size int, currency CurrencyPair) (*Depth, error) {
	resp, err := HttpGet(g.client, fmt.Sprintf("%s/orderBook/%s", marketBaseUrl, currency.ToSymbol("_")))
	if err != nil {
		errCode := HTTP_ERR_CODE
		errCode.OriginErrMsg = err.Error()
		return nil, errCode
	}

	bids, _ := resp["bids"].([]interface{})
	asks, _ := resp["asks"].([]interface{})

	dep := new(Depth)

	for _, v := range bids {
		r := v.([]interface{})
		dep.BidList = append(dep.BidList, DepthRecord{Price: ToFloat64(r[0]), Amount: ToFloat64(r[1])})
	}

	for _, v := range asks {
		r := v.([]interface{})
		dep.AskList = append(dep.AskList, DepthRecord{Price: ToFloat64(r[0]), Amount: ToFloat64(r[1])})
	}

	sort.Sort(sort.Reverse(dep.AskList))

	return dep, nil
}

func (g *Gate) GetKlineRecords(currency CurrencyPair, period, size, since int) ([]Kline, error) {
	panic("not implement")
}

//非个人，整个交易所的交易记录
func (g *Gate) GetTrades(currencyPair CurrencyPair, since int64) ([]Trade, error) {
	panic("not implement")
}

func (g *Gate) GetExchangeName() string {
	return "gate.io"
}

func (g *Gate) CancelAllOrders(orderType int, currency CurrencyPair) (bool, error) {
	var t string
	switch orderType {
	case SELLORDER:
		t = "0"
	case BUYORDER:
		t = "1"
	case ALLORDER:
		t = "-1"
	}
	p := params{
		"type":         t,
		"currencyPair": g.normalizeCurrency(currency),
	}
	url := privateUrl + "cancelAllOrders"
	data, err := g.doPrivateApi(url, p)
	if err != nil {
		return false, err
	}
	return data["result"].(string) == TRUE, nil
}
