package wex

import (
	"gitlab.com/zcong1993/GoEx"
	"net/http"
	"testing"
)

var wex = New(http.DefaultClient, "", "")

func TestWex_GetTicker(t *testing.T) {
	ticker, err := wex.GetTicker(goex.BTC_USD)
	t.Log("err=>", err)
	t.Log("ticker=>", ticker)
}
